;I2C controller for the Blue Robotics Lumen Subsea Light

    list	p=16f18313	;list directive to define processor
    #include	<p16f18313.inc>	; processor specific variable definitions
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -312     ;no  "page or bank selection not needed" messages
    errorlevel -207    ;no label after column one warning
	
    
    #define LEDread	  (b'00001111')	    ;Bus address for i2c LED controller read (d'15')
    #define LEDwrite	  (b'00001110')	    ;Bus address for i2c LED controller write (d'14')

    ; CONFIG1
; __config 0xFFFA
 __CONFIG _CONFIG1, _FEXTOSC_OFF & _CLKOUTEN_OFF & _CSWEN_ON & _FCMEN_ON
; CONFIG2
; __config 0xFFF3
 __CONFIG _CONFIG2, _MCLRE_ON & _PWRTE_OFF & _WDTE_OFF & _LPBOREN_OFF & _BOREN_ON & _BORV_LOW & _PPS1WAY_ON & _STVREN_ON & _DEBUG_OFF
; CONFIG3
; __config 0x2003
 __CONFIG _CONFIG3, _WRT_OFF & _LVP_OFF
; CONFIG4
; __config 0x3
 __CONFIG _CONFIG4, _CP_OFF & _CPD_OFF

MULTIBANK	    UDATA_SHR
w_copy		    RES 1	;variable used for context saving (work reg)
status_copy	    RES 1	;variable used for context saving (status reg)
		    
;General Variables
GENVAR		    UDATA
pclath_copy	    RES 1	;variable used for context saving (pclath copy
userMillis	    RES	1
LEDpwm		    RES	1	;PWM value to be sent to LED controller	

;**********************************************************************
    ORG		0x000	
    pagesel		start	; processor reset vector
    goto		start	; go to beginning of program
INT_VECTOR:
    ORG		0x004		; interrupt vector location
INTERRUPT:
    banksel	w_copy
    movwf       w_copy           ;save off current W register contents
    movfw        STATUS         ;move status register into W register
    movwf       status_copy      ;save off contents of STATUS register
    movf        PCLATH,W
    banksel	pclath_copy
    movwf       pclath_copy
    
    ;banksel	PORTA
    ;bsf		PORTA, 2
    
    ;Determine if STOP bit triggered interrupt
    ;banksel	SSP1STAT
    ;btfsc	SSP1STAT, P
    ;goto	LEDdone
    
    ;Check for collisions and overflows
    ;banksel	PIR1
   ; bcf		PIR1, BCL1IF
   ; banksel	SSP1CON1
   ; btfsc	SSP1CON1, SSPOV
   ; bcf		SSP1CON1, SSPOV
   ; btfsc	SSP1CON1, WCOL
   ; bcf		SSP1CON1, WCOL
    
    clrf	INTCON		;Disable interrupts for now
    banksel	PIE1
    bcf		PIE1, SSP1IE
    
    ;Read the received address from SSPBUF to clear SSPSTAT, BF flag
    banksel	SSP1BUF
    movfw	SSP1BUF
    
    banksel	PIR1
    bcf		PIR1, SSP1IF	;Clear interrupt flag
    
    ;GET lightsPWM value from master
    banksel	SSP1BUF
    movfw	SSP1BUF
    banksel	LEDpwm
    movwf	LEDpwm
    
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line
    
  
LEDdone
    banksel	PIE1
    bsf		PIE1, SSP1IE
    movlw	b'11000000'
    movwf	INTCON
    
    banksel	pclath_copy
    movfw	pclath_copy
    movwf	PCLATH
    movf	status_copy,w   ;retrieve copy of STATUS register
    movwf	STATUS          ;restore pre-isr STATUS register contents
    swapf	w_copy,f
    swapf	w_copy,w
   
    retfie
  
;******************Variable millisecond delay routuine**************************
delayMillis
    banksel	userMillis
    movwf	userMillis	;user defined number of milliseconds
startDly
    banksel	TMR0L
    clrf	TMR0L
waitTmr0
    movfw	TMR0L		;TMR0 increments once every 8uS
    xorlw	.125		;125 * 32uS = 1mS
    btfss	STATUS, Z
    goto	waitTmr0
    banksel	userMillis
    decfsz	userMillis, f	;reached user defined milliseconds yet?
    goto	startDly
    
    retlw	0
;*******************************************************************************
start:
    banksel	TRISA
    ;Set PORTS to output
    movlw	b'11111011'	
		 ;--11----	;PORTA, 4 and 5 = I2C
		 ;-----0--	'PORTA, 2 = PWM
    movwf	TRISA
    
    banksel	ANSELA
    clrf	ANSELA
;************************Configure Internal Oscillator**************************
    ;ENABLING HFINTOSC:
        ;1) Config file has set oscillator as HFINTOSC with 2x PLL (32MHz) (_RSTOSC_HFINT32)
	;2) This option can also be set via NOSC[2:0] bits of OSCON1 register.
	;      -HFINTOSC freq can be selected via HFFRQ[2:0] bits of OSCFRQ register.
	;      -NDIV[3:0] bits of OSCON1 register allow for division of clock output
	;       between 1:1 and 1:512
    ;2x PLL
    ;PLL is used with HFINTOSC clock source to provide a system clock. Input
    ;frequency to PLL is either 8,12 or 16Mhz. With 2x, this yields a system clock
    ;source of 16,24 or 32 Mhz respectively.
    ;ENABLING 2x PLL
	;1) Config file set to _RSTOSC_HFINT32
	;   -This effectively configures HFFRQ[2:0] bits of OSCFRQ register to 
	;   '110' (16Mhz) and activates the 2x PLL
	;2) Write '000' to NOSC[2:0] bits of OSCON1 register.
	;    -Then write correct value to HFFRQ[3:0] bits of OSCFRQ resiter to
	;     select desired system clock frequency
	
    movlw	b'00000000'
		 ;-000----	;NOSC=000 = HFINTOSC 32 Mhz
		 ;----0000	;NDIV=0000 = divide clock by 1
    banksel	OSCCON1
    movwf	OSCCON1
    
    movlw	b'00000011'
		 ;----0011	;OSCFRQ, HRRRQ = 0011 (Nominal freq = 4Mhz)
    banksel	OSCFRQ
    movwf	OSCFRQ
    
    movlw	b'01000000'
		 ;-1------	;OSCEN, HFOEN=1 (HFINTOSC enabled as 
				;   specified in OSCFRQ)
    banksel	OSCEN
    movwf	OSCEN
    
    banksel	OSCTUNE
    clrf	OSCTUNE		;Oscillator is operating at calibrated frequency
    
;*********************Configure TMR0********************************************
    
    movlw	b'10000000'
		 ;1-------	;T0EN=1, TMR0 module is enabled
		 ;---0----	;T016=0, TMR0 is an 8-bit timer
    banksel	T0CON0
    movwf	T0CON0
    
    movlw	b'01100101'
		 ;-11-----	;T0CS=011 (FFINTOSC) Clock source= 32Mhz internal oscillator	
				;   (This is divided by 8 in OSCCON1 register)
		 ;---0----	;T0ASYNC=0 (Input to TMR0 is synchronized to FOSC/4
				;FOSC/4=4Mhz/4=1Mhz instruction clock frequency
				;   1Mhz instr clk freq = 250 nS instr clck period.
		 ;----0101	;T0CKPS=0101 (Prescaler=1:32). TMR0 increments 
				;   once every 32 instruction cycles.
				;Instruction clock period = 250nS
				;TMR0 increments once every 8uS (32*250nS=8uS
    banksel	T0CON1
    movwf	T0CON1
    
;**********************Configure PWM********************************************
    banksel	TRISA
    bcf		TRISA, 2
    
    
    banksel	PWM5CON
    bcf		PWM5CON, PWM5POL    ;Output polarity = active-high
    
    movlw	.62            ; PR2 = 62
    banksel	PR2             ; period = (62+1)*32uS=2mS
    movwf	PR2             ; PWM frequency = 500 Hz
    
    ;PWM5DCH and PWM5DCL control duty cycle of PWM
    ;	(PWMxDC is left-justified!!!!!!)
    ;Start with default of lights off
    movlw	.34
    banksel	PWM5DCH
    movwf	PWM5DCH
    
    movlw	b'00000000'
		 ;00------
    banksel	PWM5DCL	    ;PWM5DC=b'10001100' = 136
    movwf	PWM5DCL	
    
    
    banksel	PIR1
    bcf		PIR1, TMR2IF
    
    movlw	b'00000111'     
		; -----1--      TMR2ON=1 (TMR2 is on)
		; ------11      T2CKPS=11 (Prescaler = 64)
    banksel	T2CON           ; TMR2 increments once every 32uS
    movwf	T2CON
waitT2    
    banksel	PIR1
    btfss	PIR1, TMR2IF
    goto	waitT2
    
    banksel	TRISA
    bcf		TRISA, 2	;Enable PWM5 pin output driver
    
    movlw	b'00000010'	;PORTA, 2 = PWM output (PWM5)
    banksel	RA2PPS
    movwf	RA2PPS
    
    movlw	b'10000000'
		 ;1-------	PWM5EN=1 (enable PWM module)
		 ;---0----	PWM5POL=0 (PWM output is active high)
    banksel	PWM5CON
    movwf	PWM5CON
    
;*********************Configure I2C*****************************************
    movlw	b'00001110'
		 ;-------0	;Bit 0 not used in 7-bit slave mode
		 ;0000111-	;7-bit address for i2c slave mode.  Address for
				;battery monitor will be d'2'. The 1st bye received
				;after a Start or Restart is compared to the value 
				;in this register. An interrupt is generated if match
				;occurs
    banksel	SSP1ADD
    movwf	SSP1ADD
    
    movlw	b'11111111'
		 ;-------0	;Bit 0 is not used in 7-bit slave mode
		 ;11111110-	;Mask for 7-bit i2c address sent from master. Used
				;to detect address match.
    banksel	SSP1MSK
    movwf	SSP1MSK
    
    movlw	b'10000000'
		 ;1-------	'SMP=1, data sampled at end of data output time
				;slew rate control in 100kHz mode
    banksel	SSP1STAT
    movwf	SSP1STAT
    
    movlw	b'00111110'
		 ;-0------	;SSPOV=receive overflow indicator (read data in 
				;sspbuf before new data comes in to prevent error.
				;Check and clear to ensure sspbuf can be updated
		 ;--1-----	;SSPEN=1 (Enable I2C serial port)
		 ;---1----	;Enable Clock (CKP=1)
		 ;----1110	;I2C slave mode, 7-bit address (Start and Stop interrupts enabled)
    banksel	SSP1CON1
    movwf	SSP1CON1
    
    banksel	SSP1CON3
    bsf		SSP1CON3, SCIE	;Enable interrupt on START or RESTART
    
    movlw	.10
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    
    ;Set peripheral pins
    ;Inputs
    banksel	SSP1CLKPPS
    movlw	b'00100'
    movwf	SSP1CLKPPS	;I2C clock (SCL) is PORTA, 4
    
    banksel	SSP1DATPPS
    movlw	b'00101'
    movwf	SSP1DATPPS	;I2C data line (SDA) is PORTA, 5
    ;Outputs
    movlw	b'00011000'	;PORTA, 4 = SCL for i2c
    banksel	RA4PPS
    movwf	RA4PPS
    
    movlw	b'00011001'	;PORTA, 5 = SDA for i2c
    banksel	RA5PPS
    movwf	RA5PPS
    
;******************Enable Interrupts********************************************
    movlw	b'11000000'
	         ;1-------	;Enable global interrupts (GIE=1)
		 ;-1------	;Enable peripheral interrupts (PEIE=1)
		 ;--0-----	;Disable TMR0 interrupts (TMROIE=0)
		 ;---0----	;Disable RBO/INT external interrupt (INTE=0)
		 ;----0---	;Disable interrupt on change for PORTB (IOCIE=1)
    movwf	INTCON
    
    movlw	b'00001000'
		 ;----1---	;Synchronous Serial Port (SSP) interrupts enabled.
				;Used for i2c interrupts
    banksel	PIE1
    movwf	PIE1
    
    banksel	userMillis
    clrf	userMillis
    banksel	PORTA
    clrf	PORTA
mainLoop
    ;Check to see if there needs to be any change in LED intensity. 
    ;(Prevents flickering from needlessly updating PWM5DCH)
    banksel	LEDpwm
    movfw	LEDpwm
    banksel	PWM5DCH
    xorwf	PWM5DCH, w
    btfsc	STATUS, Z
    goto	mainLoop    ;No need to update, new value is same as old value
    
    ;Update LED
    ;1st make sure the PWM value is within the range of signals acceptable by the
    ;LED. 
    movlw	.61		;(Max PWM5DCH value is 60)
    banksel	LEDpwm
    subwf	LEDpwm, w
    btfsc	STATUS, C	;C = 0 if neg result
    goto	mainLoop	;Value is too high so don't update
    
    movlw	.34
    banksel	LEDpwm
    subwf	LEDpwm, w
    btfss	STATUS, C	;C = 0 if neg result
    goto	mainLoop	;Value is too low so don't update
    
    banksel	LEDpwm
    movfw	LEDpwm
    banksel	PWM5DCH		;Value is ok to update
    movwf	PWM5DCH
    
    goto	mainLoop
    
    
   
    END                       


























