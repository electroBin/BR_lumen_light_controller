This program is designed to work with the Blue Robotics Lumen Subsea Light (2nd revision) found here:

https://bluerobotics.com/store/electronics/lights/lumen-r2-rp/

The microcontroller used is a PIC16F18313 and is designed to commumicate with main MCU of the ROV via the I2C protocol. The device receives packets which are translated into PWM values that are used to control the brightness of the LED.


